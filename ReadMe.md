## Bias-Tee

Bias-Tee is a component that blocks DC but passes RF on the RF port and passes both RF and DC on the RF+DC port.
DC voltage is applied on the jst connectors. Maximum input voltage is 16V. Output voltage
from the regulator is 5V. 

Tested assembled bias-tee:
- Input: 5V (4.9988V actual)
- Output RF+DC: 4.971V
- Output RF: 0.006V
- RF tests done with DC block on both outputs

RF Spectrum Output (RF+DC) (Input: -20dBm Trigger) is shown below:

![](https://github.com/Cand3llaX/RF-Modules-Spectra/assets/80895853/4f3d0cc1-90cc-445d-a34a-dd6462a580ef)
